import { AuthenticationActions, AuthenticationState, AuthenticationActionCreators } from 'react-aad-msal';
import { Action, Reducer } from 'redux';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface InitialAuthenticationState {
    initializing: boolean | undefined;
    initialized: boolean | undefined;
    idToken: any| null | undefined;
    accessToken: any | null | undefined;
    state: AuthenticationState | undefined;
    account: any | undefined;
}

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = AuthenticationActionCreators;

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
// Use @typeName and isActionType for type detection that works even after serialization/deserialization.

export interface InitializingAction { type: AuthenticationActions.Initializing }
export interface InitializedAction { type: AuthenticationActions.Initialized }
export interface AcquiredIdTokenSuccessAction { type: AuthenticationActions.AcquiredIdTokenSuccess, payload: any | null | undefined }
export interface AcquiredAccessTokenSuccessAction { type: AuthenticationActions.AcquiredAccessTokenSuccess, payload: any | null | undefined }
export interface AcquiredAccessTokenErrorAction { type: AuthenticationActions.AcquiredAccessTokenError }
export interface LoginSuccessAction { type: AuthenticationActions.LoginSuccess, payload: { account: any } }
export interface LoginErrorAction { type: AuthenticationActions.LoginError }
export interface AcquiredIdTokenErrorAction { type: AuthenticationActions.AcquiredIdTokenError }
export interface LogoutSuccessAction { type: AuthenticationActions.LogoutSuccess }
export interface AuthenticatedStateChangedAction { type: AuthenticationActions.AuthenticatedStateChanged, payload: AuthenticationState }

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = InitializingAction | InitializedAction | AcquiredIdTokenSuccessAction | AcquiredAccessTokenSuccessAction | AcquiredAccessTokenErrorAction | LoginSuccessAction | LoginErrorAction | AcquiredIdTokenErrorAction | LogoutSuccessAction | AuthenticatedStateChangedAction;

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const reducer: Reducer<InitialAuthenticationState> = (state: InitialAuthenticationState | undefined, incomingAction: Action): InitialAuthenticationState => {
    if (state === undefined) {
        return {} as InitialAuthenticationState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
      case AuthenticationActions.Initializing:
        return {
          ...state,
          initializing: true,
          initialized: false,
        };
      case AuthenticationActions.Initialized:
        return {
          ...state,
          initializing: false,
          initialized: true,
        };
      case AuthenticationActions.AcquiredIdTokenSuccess:
        return {
          ...state,
          idToken: action.payload,
        };
      case AuthenticationActions.AcquiredAccessTokenSuccess:
        return {
          ...state,
          accessToken: action.payload,
        };
      case AuthenticationActions.AcquiredAccessTokenError:
        return {
          ...state,
          accessToken: null,
        };
      case AuthenticationActions.LoginSuccess:
        return {
          ...state,
          account: action.payload.account,
        };
      case AuthenticationActions.LoginError:
      case AuthenticationActions.AcquiredIdTokenError:
      case AuthenticationActions.LogoutSuccess:
        return { ...state, idToken: null, accessToken: null, account: null };
      case AuthenticationActions.AuthenticatedStateChanged:
        return {
          ...state,
          state: action.payload,
        };
      default:
        return state;
    }
};